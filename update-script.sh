#!/bin/bash

docker rm -f $(docker ps -aq) && docker rmi -f $(docker images -q)
docker pull registry.gitlab.com/public-jalen/dockerbuildimage/profilepage:latest
docker run -d --name profilepage -p 8081:80 registry.gitlab.com/public-jalen/dockerbuildimage/profilepage:latest
rm -rf /tmp/repo
