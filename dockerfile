FROM nginx:latest

WORKDIR /usr/share/nginx/html

COPY content/* .

CMD ["nginx", "-g", "daemon off;"]
