# Build your docker image with CI/CD pipeline 
1. Testing Env
![alt text](https://i.imgur.com/1veZCFp.png)

2. Production Env
![alt text](https://i.imgur.com/miq4biA.png)


## Getting started

This is a simple CI/CD pipeline for you to build a docker image from the Dockerfile. 

The testing environment will deploy the webpage on test.jaronity.com, while the production environment will deploy the webpage on prod.jaronity.com.

## Step 1 
We will build the image from the Dockerfile and save it in the GitLab container registry.

## Step 2
Afterward, the image will be fetched and launched as a Docker container on my Gitlab-Runner server, which acts as the testing server. A curl test is performed to ensure that the webpage is function properly.

## Step 3
A manual step will be executed to generate an official Docker image from the testing image and push it to the registry. Subsequently, all Docker images/containers on my GitLab runner testing server will be destroyed and removed.

## Step 4
On the production server, the official Docker image will be will be fetched and launched as a container, and to verify the proper functioning of the webpage, a curl test will also be performed.